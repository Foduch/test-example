import time
import allure
from selenium.webdriver.common.keys import Keys
from allure_commons.types import AttachmentType

import config


class Element:

    def __init__(self, element, xpath=None):
        self.element = element
        self.xpath = xpath


class Wrapper:

    def __init__(self, driver):
        self.driver = driver

    @allure.step('{message}')
    def log(self, message):
        pass

    def wait_until(self, condition, message=None, timeout=None):
        if timeout is None:
            timeout = config.wait_timeout
        end = time.time() + timeout
        while time.time() < end:
            try:
                if condition():
                    return
            except:
                pass
            time.sleep(0.25)
        raise Exception(message if message else 'Timeout exception')

    def find_by_xpath(self, xpath):
        self.log(f'Ищем элемент "{xpath}"')
        self.wait_until(lambda : self.driver.find_element_by_xpath(xpath), message=f'Element {xpath} steel not exist after {config.wait_timeout} second')
        return Element(self.driver.find_element_by_xpath(xpath), xpath=xpath)

    def find_elements_by_xpath(self, xpath):
        return [Element(e, xpath) for e in self.driver.find_elements_by_xpath(xpath)]

    def get_attribute(self, element, attribute):
        self.wait_for_displayed(element)
        return element.element.get_attribute(attribute)

    def get_value(self, element):
        return self.get_attribute(element, 'value')

    def set_value(self, element, value):
        while len(self.get_value(element)) > 0:
            element.element.send_keys(Keys.BACKSPACE)
        element.element.clear()
        self.log(f'Вводим значение "{value}" в поле "{element.xpath}"')
        element.element.send_keys(value)

    def check_value(self, element, value):
        element_value = self.get_value(element)
        self.log(f'Проверяем значение {value} в элементе "{element.xpath}"')
        assert element_value == value

    def click(self, element):
        self.log(f'Кликаем по элементу "{element.xpath}"')
        element.element.click()

    def wait_for_not_displayed(self, element):
        self.log(f'Ожидаем исчезновение элемента "{element.xpath}"')
        self.wait_until(lambda : not element.element.is_displayed())

    def check_not_exist(self, xpath, timeout=None):
        if timeout is None:
            timeout = config.wait_timeout
        end = time.time() + timeout
        while time.time() < end:
            try:
                self.driver.find_element_by_xpath(xpath)
            except:
                return
            time.sleep(0.25)
        raise Exception(f'Element {xpath} steel displayed')

    def wait_for_displayed(self, element):
        self.log(f'Ожидаем отображение элемента {element.xpath}')
        self.wait_until(lambda : element.element.is_displayed())

    def pause(self, timeout=None):
        time.sleep(timeout if timeout else 1)

    def attach_screenshot(self):
        allure.attach(self.driver.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)

    def check_text(self, element, text):
        actual_text = self.get_attribute(element, 'innerText').strip()
        assert actual_text == text
