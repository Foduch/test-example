import allure
import requests
import json

import config


class BaseApiWrapper:

    @allure.step('{method} {uri}')
    def _execute(self, method, uri, headers=None, data=None):
        return requests.request(method, f'{config.base_uri}{uri}', headers=headers, data=data)

    def get(self, uri, headers=None):
        return self._execute('GET', uri, headers=headers)

    def post_json(self, uri, data, headers=None):
        json_data = json.dumps(data)
        return self._execute('POST', uri, data=json_data)

    def delete(self, uri, headers=None):
        return self._execute('DELETE', uri)
