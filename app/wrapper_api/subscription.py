from .base import BaseApiWrapper

class SubscriptionApi(BaseApiWrapper):

    def add_subscription(self, data):
        response = self.post_json('/subscriptions', data)
        return response

    def get_list(self):
        response = self.get('/subscriptions')
        return response

    def delete_all(self):
        response = self.delete('/subscriptions')
        return response
