from .base import BasePage


DEFAULT_TIME = '7d'


class SubscriptionPage(BasePage):

    def __init__(self, driver):
        super(SubscriptionPage, self).__init__(driver)

    @property
    def time_field(self):
        return self.wrap.find_by_xpath('//input[@name="time"]')

    @property
    def email_field(self):
        return self.wrap.find_by_xpath('//input[@name="email"]')

    @property
    def name_field(self):
        return self.wrap.find_by_xpath('//input[@name="name"]')

    @property
    def add_button(self):
        return self.wrap.find_by_xpath('//div[@data-test="new-subs-form"]//button')

    @property
    def list_items(self):
        return self.wrap.find_elements_by_xpath('//tr[@data-test="subs-table-row"]')

    def get_sub(self, text):
        return self.wrap.find_by_xpath(f'//tr[@data-test="subs-table-row"][td[normalize-space(text())="{text}"]]')

    def get_sub_name(self, text):
        return self.wrap.find_by_xpath(f'//tr[@data-test="subs-table-row"][td[normalize-space(text())="{text}"]]//th[@data-test="subs-table-name"]')


    def open(self):
        self.url('/ui')

    def wait_loaded(self):
        sub_list = self.wrap.find_by_xpath('//div[@data-test="subs-list"]')
        desc = self.wrap.find_by_xpath('//div[@data-test="description"]')
        self.wrap.wait_for_displayed(sub_list)
        self.wrap.wait_for_displayed(desc)

    def check_default_time(self):
        self.wrap.check_value(self.time_field, DEFAULT_TIME)

    def wait_empty_list(self):
        self.log('Ожидаем пустой список')
        self.wrap.wait_until(lambda : len(self.list_items) == 0, message='Не дождались пустого списка')

    def delete_all(self):
        self.wrap.click(self.wrap.find_by_xpath('//button[@data-test="clear-button"]'))

    def add_subscription(self, email, name, time):
        self.wrap.set_value(self.email_field, email)
        self.wrap.set_value(self.name_field, name)
        self.wrap.set_value(self.time_field, time)
        self.wrap.attach_screenshot()
        self.wrap.click(self.add_button)
        self.wrap.wait_until(lambda : self.wrap.get_value(self.time_field) == DEFAULT_TIME)

    def wait_sub(self, text):
        self.wrap.wait_for_displayed(self.get_sub(text))

    def check_sub_active(self, text):
        self.wait_sub(text)
        self.wrap.wait_for_displayed(self.wrap.find_by_xpath(f'{self.get_sub(text).xpath}//*[@data-icon="check"]'))

    def check_sub_not_active(self, text):
        self.wait_sub(text)
        self.wrap.wait_for_displayed(self.wrap.find_by_xpath(f'{self.get_sub(text).xpath}//*[@data-icon="times"]'))

    def check_sub_name(self, email, name):
        self.wait_sub(email)
        self.wrap.check_text(self.get_sub_name(email), name)

    def wait_no_sub(self, text):
        self.wrap.check_not_exist(f'//tr[@data-test="subs-table-row"][td[normalize-space(text())="{text}"]]')
