from config import base_uri
import allure
from app.wrapper_ui import Wrapper


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.wrap = Wrapper(driver)

    def url(self, url):
        self.log(f'Переходим по адресу "{base_uri + url}"')
        self.driver.get(base_uri + url)

    @allure.step('{message}')
    def log(self, message):
        pass
