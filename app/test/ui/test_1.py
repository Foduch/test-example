import selenium
from pytest import fixture
from app.page import SubscriptionPage


@fixture
def sub_page(selenium):
    page = SubscriptionPage(selenium)
    page.open()
    page.wait_loaded()
    yield page


class TestSub:

    def test_add_sub(self, sub_page):
        sub_page.check_default_time()
        sub_page.add_subscription('email@test.qa', 'name', '7d')
        sub_page.check_sub_active('email@test.qa')
        
    def test_add_empty_time(self, sub_page):
        sub_page.add_subscription('empty@time.qa', 'name', '')
        sub_page.check_sub_not_active('empty@time.qa')
        
    def test_add_empty_name(self, sub_page):
        sub_page.add_subscription('empty@name.qa', '', '1d')
        sub_page.check_sub_name('empty@name.qa', '')
        
    def test_add_empty_email(self, sub_page):
        sub_page.add_subscription('', 'empty email', '1d')
        sub_page.wait_no_sub('empty email')
        
    def test_delete_subs(self, sub_page):
        sub_page.delete_all()
        sub_page.wait_empty_list()