from pytest import fixture
import selenium
import allure
from allure_commons.types import AttachmentType
import socket


@fixture(autouse=True)
def screenshot(selenium):
    yield
    allure.attach(selenium.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)

@fixture
def chrome_options(chrome_options):
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    return chrome_options

@fixture
def firefox_options(firefox_options):
    firefox_options.add_argument('--no-sandbox')
    firefox_options.add_argument('--disable-dev-shm-usage')
    return firefox_options
