from pytest import fixture, mark
from datetime import datetime, timedelta

from app.wrapper_api import SubscriptionApi

TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

@fixture(scope='class')
def api():
    yield SubscriptionApi()
    pass


class TestSubscription:

    def test_create(self, api):
        correct_subscription = {'email': 'email@test.qa', 'name': 'Correct subscription name', 'time': '2d'}
        response = api.add_subscription(correct_subscription)
        response_data = response.json()
        assert response.status_code == 200
        assert 'id' in response_data
        id = response_data['id']
        assert len(id) > 0
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        assert len(matches) == 1
        added_subscription = matches[0]
        assert added_subscription['email'] == correct_subscription['email']
        assert added_subscription['name'] == correct_subscription['name']

    def test_name(self, api):
        correct_subscription = {'email': 'name@test.qa', 'name': 'Name test subscription name', 'time': '2d'}
        response = api.add_subscription(correct_subscription)
        response_data = response.json()
        assert response.status_code == 200
        assert 'id' in response_data
        id = response_data['id']
        assert len(id) > 0
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        assert len(matches) == 1
        added_subscription = matches[0]
        assert added_subscription['name'] == correct_subscription['name']

    def test_email(self, api):
        correct_subscription = {'email': 'name@test.qa', 'name': 'Name test subscription name', 'time': '2d'}
        response = api.add_subscription(correct_subscription)
        response_data = response.json()
        assert response.status_code == 200
        assert 'id' in response_data
        id = response_data['id']
        assert len(id) > 0
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        assert len(matches) == 1
        added_subscription = matches[0]
        assert added_subscription['email'] == correct_subscription['email']

    def test_creation_time(self, api):
        correct_subscription = {'email': 'creation@time.qa', 'name': 'Creation time subscription name', 'time': '2d'}
        posted_time = datetime.utcnow()
        response = api.add_subscription(correct_subscription)
        assert response.status_code == 200
        id = response.json()['id']
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        created_at = datetime.strptime(matches[0]['created_at'], TIME_FORMAT)
        assert abs(posted_time - created_at) < timedelta(minutes=1)

    def test_expire_time(self, api):
        correct_subscription = {'email': 'expire@time.qa', 'name': 'Expire time subscription name', 'time': '2d'}
        posted_time = datetime.utcnow()
        response = api.add_subscription(correct_subscription)
        assert response.status_code == 200
        id = response.json()['id']
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        expired_at = datetime.strptime(matches[0]['expired_at'], TIME_FORMAT)
        expected_expire_date = (posted_time + timedelta(days=2))
        assert abs(expected_expire_date - expired_at) < timedelta(minutes=1)

    @mark.skip()
    def test_create_first_level_domain(self, api):
        first_level_domain = {'email': 'email@test', 'name': 'First level domain subscription name', 'time': '2d'}
        response = api.add_subscription(first_level_domain)
        response_data = response.json()
        assert response.status_code == 200
        assert 'id' in response_data
        assert len(response_data['id']) > 0

    def test_email_require(self, api):
        no_email = {'name': 'No email subscription name', 'time': '2d'}
        response = api.add_subscription(no_email)
        response_data = response.json()
        assert response.status_code == 200
        assert 'error' in response_data
        assert "Field is required: ['email']" in response_data['error']

    def test_no_valid_email(self, api):
        not_valid_email = {'email': 'not-valid', 'name': 'No email subscription name', 'time': '2d'}
        response = api.add_subscription(not_valid_email)
        response_data = response.json()
        assert response.status_code == 200
        assert 'error' in response_data
        assert f"Invalid email address: {not_valid_email['email']}: ['email']" in response_data['error']

    def test_empty_name(self, api):
        empty_name = {'email': 'empty@name.qa', 'name': '', 'time': '2d'}
        response = api.add_subscription(empty_name)
        response_data = response.json()
        assert response.status_code == 200
        id = response_data['id']
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        assert matches[0]['name'] == ''

    def test_name_require(self, api):
        no_name = {'email': 'no@name.qa', 'time': '2d'}
        response = api.add_subscription(no_name)
        response_data = response.json()
        assert response.status_code == 200
        assert "Field is required: ['name']" in response_data['error']

    @mark.skip('500 error')
    def test_time_require(self, api):
        no_time = {'email': 'no@time.qa', 'name': 'No time'}
        response = api.add_subscription(no_time)
        response_data = response.json()
        assert response.status_code == 200
        assert "Field is required: ['time']" in response_data['error']

    def test_empty_time(self, api):
        empty_time = {'email': 'no@time.qa', 'name': 'Empty time', 'time': ''}
        posted_time = datetime.utcnow()
        response = api.add_subscription(empty_time)
        assert response.status_code == 200
        id = response.json()['id']
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        expired_at = datetime.strptime(matches[0]['expired_at'], TIME_FORMAT)
        assert abs(posted_time - expired_at) < timedelta(minutes=1)

    def test_negative_time(self, api):
        negative_time = {'email': 'no@time.qa', 'name': 'Empty time', 'time': '-1d'}
        posted_time = datetime.utcnow()
        response = api.add_subscription(negative_time)
        assert response.status_code == 200
        id = response.json()['id']
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        expired_at = datetime.strptime(matches[0]['expired_at'], TIME_FORMAT)
        assert abs(posted_time - expired_at) < timedelta(minutes=1)

    def test_comment(self, api):
        with_comment = {'email': 'with@comment.qa', 'name': 'With comment', 'time': '2d', 'comment': 'comment'}
        response = api.add_subscription(with_comment)
        response_data = response.json()
        assert response.status_code == 200
        assert 'id' in response_data
        id = response_data['id']
        subscription_list = api.get_list().json()
        matches = [item for item in subscription_list if item['id'] == id]
        assert 'comment' in matches[0]
        assert matches[0]['comment'] == with_comment['comment']

    def test_delete_subscriptions(self, api):
        response = api.delete_all()
        assert response.status_code == 200
        response_data = response.json()
        assert 'removed' in response_data
        subscription_list = api.get_list().json()
        assert len(subscription_list) == 0
