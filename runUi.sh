BROWSER=$1


rm -rf allure_ui_results/*
source env/bin/activate
if [[ "$BROWSER" = "" ]] || [[ "$BROWSER" = "Chrome" ]]
then 
  python -m pytest --alluredir=allure_ui_results app/test/ui/** --driver=Chrome --driver-path=./chromedriver
fi
if [[ "$BROWSER" = "Firefox" ]]
then
  python -m pytest --alluredir=allure_ui_results app/test/ui/** --driver=Firefox --driver-path=./geckodriver
fi
allure generate --clean ./allure_ui_results --output allure-ui-report
deactivate