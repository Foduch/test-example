rm -rf allure_api_results/*
source env/bin/activate
python -m pytest --alluredir=allure_api_results app/test/api/**
allure generate --clean ./allure_api_results --output allure-api-report
deactivate