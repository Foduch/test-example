# test-example

## Требования
- python 3.6+
- nodejs 12+
- allure-command-line global installed npm package
- firefox 75
- chrome 81

## Запуск
- создать виртуальное окружения
- установить пакеты из requirements.txt
- запустить api тесты скриптом runApi.sh
- запустить ui тесты скриптом runUi.sh 

## Отчеты
- allure-api-report/index.html
- allure-ui-report/index.html